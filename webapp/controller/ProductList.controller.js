sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"parasus/products/model/formatter",
	"sap/ui/core/Fragment"
], function (Controller, Filter, FilterOperator, formatter, Fragment) {
	"use strict";

	return Controller.extend("parasus.products.controller.ProductList", {

		formatter: formatter,

		onInit: function () {
			var oData = {
				"name": "Rahul"
			};
			var oJsonModel = new sap.ui.model.json.JSONModel(oData);
			this.getView().setModel(oJsonModel, "jsonModel");
		},

		onProductIDSubmit: function (oEvent) {

		},

		onPressGo: function (oEvent) {
			var sProId = this.getView().byId("idProductInput").getValue();
			var sProName = this.getView().byId("idProductNameInput").getValue();
			var aTokensId = this.getView().byId("idProductInput").getTokens();
			var aTokensName = this.getView().byId("idProductNameInput").getTokens();
			var oFilter = [];
			var oFilter1;

			// code for searching in product id only
			function searchForID(sProId, aTokensId) {
				var oFilter = [];
				var oFilter1;

				if (sProId !== "") {
					oFilter1 = new Filter({
						path: 'ProductID',
						operator: FilterOperator.EQ,
						value1: sProId
					});
					oFilter.push(oFilter1);
				}

				if (aTokensId.length > 0) {
					aTokensId.forEach(function (aTokenId) {
						var vkey = aTokenId.getKey();
						oFilter1 = new Filter({
							path: 'ProductID',
							operator: FilterOperator.EQ,
							value1: vkey
						});
						oFilter.push(oFilter1);
					});
				}

				return oFilter;
			}

			// code for searching in product name only
			function searchForName(sProName, aTokensName) {
				var oFilter = [];
				var oFilter1;

				if (sProName !== "") {
					oFilter1 = new Filter({
						path: 'ProductName',
						operator: FilterOperator.Contains,
						value1: sProName
					});
					oFilter.push(oFilter1);
				}

				if (aTokensName.length > 0) {
					aTokensName.forEach(function (aTokenName) {
						var vText = aTokenName.getText();
						var vText1 = vText.split("(")[0].trim();
						oFilter1 = new Filter({
							path: 'ProductName',
							operator: FilterOperator.Contains,
							value1: vText1
						});
						oFilter.push(oFilter1);
					});
				}
				return oFilter;
			}

			if (sProName === "" && aTokensName.length === 0) {
				// code for searching in product id only
				var oNewFilterID = searchForID(sProId, aTokensId);
				oFilter = oNewFilterID;

			} else if (sProId === "" && aTokensId.length === 0) {
				// code for searching in product Name only
				var oNewFilterName = searchForName(sProName, aTokensName);
				oFilter = oNewFilterName;

			} else {
				// code for searching in both the cases
				// var oFilter = new Filter({
				// 	filters: [
				// 		new Filter({
				// 			path: 'ProductID',
				// 			operator: FilterOperator.EQ,
				// 			value1: sProId
				// 		}),

				// 		new Filter({
				// 			path: 'ProductName',
				// 			operator: FilterOperator.Contains,
				// 			value1: sProName
				// 		})
				// 	],
				// 	and: true
				// });

				var oNewFilterID = searchForID(sProId, aTokensId);
				var oNewFilterName = searchForName(sProName, aTokensName);

				oFilter = new Filter({
					filters: [
						new Filter(oNewFilterID),
						new Filter(oNewFilterName)
					],
					and: true

				});

			}

			var oTable = this.getView().byId("idTable");
			oTable.getBinding("items").filter(oFilter);

		},
		
		onClearBtn: function (oEvent) {
			this.getView().byId("idProductInput").removeAllTokens();
			this.getView().byId("idProductNameInput").removeAllTokens();
		},

		onRowClick: function (oEvent) {
			var sProductId = oEvent.getParameter("listItem").getAggregation("cells")[0].getTitle();
			this.getOwnerComponent().getRouter().navTo("RouteProductDetail", {
				productId: sProductId
			});
		},

		// value help
		onProductIDVHRequest: function () {

			// old version on how to call fragment
			// if (!this._oValueHelpDialog) {
			// 	this._oValueHelpDialog = sap.ui.xmlfragment("parasus.products.Fragment.ProductNameVH", this);
			// }
			// this.getView().addDependent(this._oValueHelpDialog);
			// this._oValueHelpDialog.open();

			// new version on how to call fragment
			var oView = this.getView();
			this._oMultiInput = this.getView().byId("idProductInput");
			var that = this;
			if (!this.byId("valHelpDialog")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					name: "parasus.products.Fragment.ProductNameVH",
					controller: this
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					that.fetchData();
					oDialog.open();
				});
			} else {
				// this.fetchData();
				this.byId("valHelpDialog").open();
			}

			// bind value help table
			// this._oValueHelpDialog.getTableAsync().then(function (oTable) {
			// 		oTable.setModel(this.getView().getModel());
			// oTable.setModel(this.oColModel, "columns");

			// if (oTable.bindRows) {
			// 	oTable.bindAggregation("rows", "/ProductCollection");
			// }

			// 	if (oTable.bindItems) {
			// 		oTable.bindAggregation("items", "/Products", function () {
			// 			return new sap.m.ColumnListItem({
			// 				cells: aCols.map(function (column) {
			// 					return new Label({ text: "{" + column.template + "}" });
			// 				})
			// 			});
			// 		});
			// 	}
			// 	this._oValueHelpDialog.update();
			// }.bind(this));
		},

		// value help  """""""xyz
		onProductNameVHRequest: function () {

			var oView = this.getView();
			this._oMultiInput = this.getView().byId("idProductNameInput");
			var that = this;
			if (!this.byId("valHelpDialog")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					name: "parasus.products.Fragment.ProductNameVH",
					controller: this
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					that.fetchData();
					oDialog.open();
				});
			} else {
				// this.fetchData();
				this.byId("valHelpDialog").open();
			}
		},
		// """""""xyz

		fetchData: function () {
			this.getView().getModel().read("/Products", {
				success: this.bindTable.bind(this),
				error: this.errorHandler
			});
		},

		bindTable: function (data) {
			var oVHModel = new sap.ui.model.json.JSONModel(data, "vhModel");

			// this._oValueHelpDialog.getTableAsync().then(function (oTable) {
			this.byId("valHelpDialog").getTableAsync().then(function (oTable) {
				oTable.setModel(oVHModel);
				oTable.addColumn(new sap.ui.table.Column({
					label: "Product ID",
					template: "ProductID"

				}));
				oTable.addColumn(new sap.ui.table.Column({
					label: "Product Name",
					template: "ProductName"
				}));

				oTable.bindRows("/results/");
				// this._oValueHelpDialog.update();
				this.byId("valHelpDialog").update();

			}.bind(this));
			// this.getView().byId("idProductInput").setTokens(this.byId("valHelpDialog").getTokens());
		},

		errorHandler: function () {
			console.log("some error has occured while fetching the data");
		},

		onValueHelpOkPress: function (oEvent) {
			var aTokens = oEvent.getParameter("tokens");
			// this.getView().byId("idProductInput").setTokens(aTokens);
			this._oMultiInput.setTokens(aTokens);
			this.byId("valHelpDialog").close();
		},

		onValueCancelPress: function () {
			// this._oValueHelpDialog.close();
			this.byId("valHelpDialog").close();
		},
		
		sum: function(a ,b){
			return a + b;
		},
		
		

		// onValueHelpAfterClose: function () {
		// 	this.byId("valHelpDialog").destroy();
		// },

		// onProductNameVHRequest: function () {

		// }

	});
});