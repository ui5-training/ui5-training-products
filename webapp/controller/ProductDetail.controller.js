sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"parasus/products/model/formatter",
], function (Controller, formatter) {
	"use strict";

	return Controller.extend("parasus.products.controller.ProductDetail", {
		
		formatter: formatter,

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf parasus.products.view.ProductDetail
		 */
		onInit: function () {
			this.getOwnerComponent().getRouter().getRoute("RouteProductDetail").attachMatched(this._onRouteMatched, this);

		},
		
		_onRouteMatched: function(oEvent){
			var productId = oEvent.getParameter("arguments").productId;
			this.getView().bindElement("/Products("+ productId +")");
		}   

	});

});