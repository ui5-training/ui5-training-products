/*global QUnit*/

sap.ui.define([
	"parasus/products/controller/ProductList.controller"
], function (Controller) {
	"use strict";

	QUnit.module("ProductList Controller");
	
	var oAppController = new Controller();
	
	QUnit.test("Test errorHandler and instantion", function (assert) {
		// oAppController.onInit();
		oAppController.errorHandler();
		assert.ok(oAppController, "PLC instantiated successfully");
	});

	QUnit.test("Test for sum function", function (assert) {
		var iSum = oAppController.sum(2, 3);
		assert.strictEqual(iSum, 5, "actual" + iSum + ", Expected: 5");
	});

});