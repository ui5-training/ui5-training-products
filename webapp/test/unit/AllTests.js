sap.ui.define([
	"parasus/products/test/unit/controller/ProductList.controller",
	"parasus/products/model/formatter",
], function (Controller, formatter) {
	"use strict";
	
	QUnit.module("Price with Currency");
	
	QUnit.test("should format the price with Decimals", function (assert) {
		
		// Action
		var sFormattedPrice = formatter.appendCurrency("42");
		
		// Assertion
		assert.equal(sFormattedPrice, "42.00", "The price is formatted");
		
	});
});